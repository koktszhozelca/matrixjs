class Matrix {
  constructor(row, col) {
    this.row = row;
    this.col = col;
    this.data = new Object();
  }

  static restore(obj){
    var m = new Matrix(obj.row, obj.col);
    m.data = obj.data;
    return m;
  }

  //Read, Write, Update, fill operations
  read(row, col){
    var val = this.data[row+"_"+col];
    return val ? val : 0;
  }

  readSlot(key){
    var val = this.data[key];
    return val ? val : 0;
  }

  readAll(callback){
    for(var row=0; row<this.row; row++)
      for(var col=0; col<this.col; col++)
        callback(this.read(row, col), row, col);
    return this;
  }

  write(data){
    if(this.validate(this).full)
      throw "Error: It's already fulled.";
    this.data[this.nextSlot()] = data;
    return this;
  }

  writeAll(...data){
    for(var i in data) this.write(data[i]);
    return this;
  }

  update(row, col, data){
    this.data[row+"_"+col] = data;
    return this;
  }

  updateAll(newValue){
    return this.readAll((data, row, col)=>
      this.update(row,col,newValue));
  }

  //Matrix operations
  transpose(){
    var m = new Matrix(this.col, this.row);
    var col = 0, key;
    do{
      key = "0_"+col;
      do {
        m.write(this.readSlot(key));
        key = this.nextRow(key);
      } while(key)
    } while(++col < this.col);
    return m;
  }

  add(objOrVal){
    return objOrVal.constructor.name === "Matrix" ?
      this.addMatrix(objOrVal) : this.addConst(objOrVal);
  }

  addConst(val){
    return this.readAll((data, row, col)=>
      this.update(row,col, data+val))
  }

  addMatrix(matrix){
    if(!this.validate(matrix).add)
      throw "Error: Dimension do not match.";
    return this.readAll((data, row, col)=>
      this.update(row, col, data + matrix.read(row, col)))
  }

  subtract(objOrVal){
    return objOrVal.constructor.name === "Matrix" ?
      this.subtractMatrix(objOrVal) : this.subtractConst(objOrVal);
  }

  subtractConst(val){
    return this.readAll((data, row, col)=>
      this.update(row,col, data-val))
  }

  subtractMatrix(matrix){
    if(!this.validate(matrix).add)
      throw "Error: Dimension do not match.";
    return this.readAll((data, row, col)=>
      this.update(row, col, data - matrix.read(row, col)))
  }

  multiply(objOrVal){
    return objOrVal.constructor.name === "Matrix" ?
      this.multiplyMatrix(objOrVal) : this.multiplyConst(objOrVal);
  }

  multiplyConst(val){
    return this.readAll((data, row, col)=>
      this.update(row,col, data*val))
  }

  multiplyMatrix(matrix){
    if(!this.validate(matrix).multiply)
      throw "Error: Dimension do not match.";
    var matrixT = matrix.transpose(), vecA, vecB;
    var m = new Matrix(this.row, matrix.col);
    for(var i=0; i<this.row; i++)
      for(var j=0; j<matrixT.row; j++)
        m.write(this.dotProduct(this.readRow(i), matrixT.readRow(j)))
    return this.transfer(m);
  }

  entrywiseProduct(matrix){
    if(!this.validate(matrix).add)
      throw "Error: Dimension do not match.";
    return this.readAll((data, row, col)=>
      this.update(row, col, data * matrix.read(row, col)))
  }

  //Others
  passFunc(func){
    return this.readAll((data, row, col)=>
      this.update(row, col, func(data, row, col)))
  }

  transfer(matrix){
    this.row = matrix.row;
    this.col = matrix.col;
    this.data = JSON.parse(JSON.stringify(matrix.data));
    return this;
  }

  nextRow(key){
    var [row, col] = key.split("_");
    if(parseInt(row) >= parseInt(this.row)-1) return false;
    return (parseInt(row)+1) + "_" + col;
  }

  readRow(row){
    var data = [];
    for(var i=0; i<this.col; i++)
      data.push(this.data[row+"_"+i])
    return data;
  }

  dotProduct(vecA, vecB){
   var sum = 0;
   for(var i=0; i<Math.min(vecA.length, vecB.length); i++)
     sum += vecA[i] * vecB[i];
   return sum;
  }

  parseData(data, round){
    var type = (typeof data).toString().toLowerCase();
    if(round && type === "number") return this.round(data).toString();
    if(type === "object") return JSON.stringify(data);
    return data.toString();
  }

  print(round=true){
    var cur = 0, row, col;
    this.readAll((data, row, col)=>{
      if(row !== cur) {
        this.out("\n")
        cur = row;
      }
      var data = this.parseData(data, round);
      this.out(data.padEnd(data.length+Math.abs((round? 10 : 25)-data.length)));
    })
    this.out("\n\n")
    return this;
  }

  round(val){
    return Math.round(val*1000)/1000;
  }

  out(msg){
    process.stdout.write(msg);
  }

  nextSlot(){
    var size = this.size();
    var col = size % this.col;
    var row = Math.floor(size / this.col);
    return row+"_"+col;
  }

  clone(){
    return new Matrix(this.row, this.col).writeAll(...JSON.parse(JSON.stringify(Object.values(this.data))));
  }

  validate(matrix=null){
    return {
      multiply: matrix ? this.col === matrix.row : false,
      add: matrix ? this.row === matrix.row && this.col === matrix.col : false,
      full: this.size() === this.row * this.col
    }
  }

  space(){
    return this.row * this.col;
  }

  size(){
    return Object.keys(this.data).length;
  }
}
module.exports = Matrix;
